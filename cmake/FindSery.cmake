# Try to find the Sery library
#
# This module defines the following variables:
# 
# SERY_FOUND            - If Sery was found
# SERY_INCLUDE_DIR      - The include directory of Sery
# SERY_LIBRARY          - Link against this variable
# SERY_LIBRARY_DEBUG    - Contains the debug library if found. Otherwise, contains the release.
# SERY_LIBRARY_RELEASE  - Contains the release library if found. Otherwise, contains the debug.
# 
# This module will use the following variables:
# 
# SERY_ROOT - This will be used to find Sery
#

# Defines potential paths for finding Sery
SET(SERY_FIND_PATHS
  ${SERY_ROOT}
  "C:/Program Files (x86)/sery"
  "C:/Program Files/sery"
  $ENV{SERY_ROOT}
  /usr/local/
  /usr/
  /sw
  /opt/local/
  /opt/csw/
  /opt/
)

if (NOT SERY_FIND_QUIETLY)
  message(STATUS "Looking for Sery...")
endif ()

# Look for include folder
find_path(SERY_INCLUDE_DIR NAMES Sery/Stream.hh
  HINTS
    ${SERY_FIND_PATHS}
  PATH_SUFFIXES include)

# Look for debug library
find_library(SERY_LIBRARY_DEBUG
  NAMES         sery_d
  PATH_SUFFIXES lib64 lib
  PATHS         ${SERY_FIND_PATHS})

# Look for release library
find_library(SERY_LIBRARY_RELEASE
  NAMES         sery
  PATH_SUFFIXES lib64 lib
  PATHS         ${SERY_FIND_PATHS})

# If at least one library was found
if (SERY_LIBRARY_DEBUG OR SERY_LIBRARY_RELEASE)

  # Mark as found
  SET(SERY_FOUND TRUE)

  if (SERY_LIBRARY_DEBUG AND SERY_LIBRARY_RELEASE)
    # Both libraries were found
    SET(SERY_LIBRARY
      debug     ${SERY_LIBRARY_DEBUG}
      optimized ${SERY_LIBRARY_RELEASE})

  elseif(SERY_LIBRARY_DEBUG)
    # Only debug version was found
    SET(SERY_LIBRARY ${SERY_LIBRARY_DEBUG})
    SET(SERY_LIBRARY_RELEASE ${SERY_LIBRARY_DEBUG})

  elseif(SERY_LIBRARY_RELEASE)
    # Only release version was found
    SET(SERY_LIBRARY ${SERY_LIBRARY_RELEASE})
    SET(SERY_LIBRARY_DEBUG ${SERY_LIBRARY_RELEASE})
  endif()
else()
  # Sery was not found
  SET(SERY_FOUND FALSE)
endif()

# Don't show variables to user
mark_as_advanced(
  SERY_INCLUDE_DIR
  SERY_LIBRARY
  SERY_LIBRARY_RELEASE
  SERY_LIBRARY_DEBUG
)

if (SERY_FOUND)
  message(STATUS "-- Found Sery : ${SERY_LIBRARY}")
else()
  if(SERY_FIND_REQUIRED)
    # Fatal error
    message(FATAL_ERROR "Could NOT find Sery")
  elseif(NOT SERY_FIND_QUIETLY)
    # Error, but continue
    message("Could NOT find Sery")
  endif()
endif()