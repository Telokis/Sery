Sery
======

[![Linux Build][travis-image]][travis-url]
[![Windows Build][appveyor-image]][appveyor-url]
[![Coverage Status][coveralls-image]][coveralls-url]

Synopsis
------

**Sery** is a serialization library meant to exclusively work with binary data and without being intrusive at all: you don't have to change your classes to use **Sery**. You can simply respect encapsulation by using getters/setters.
**Sery** allows you to easily convert your objects to binary buffers to send them on the network or to write them in a binary file.

Usage example
------

The following sample serializes a simple 32-bit magic number and a std::string :

```c++
#include <Sery/Buffer>
#include <Sery/Stream>
#include <fstream>

int             main()
{
  Sery::Buffer  buffer;
  Sery::Stream  stream(buffer, Sery::LittleEndian);
  std::ofstream file("BinaryFile", std::ofstream::binary);
  Sery::int32   magic = 0x11223344;
  std::string   str("Hello world!");

  stream << magic << str;
  file.write(buffer.data(), buffer.size());
  file.close();
}
```

And the following example reads from the file and checks the magic number :

```c++
#include <Sery/Buffer>
#include <Sery/Stream>
#include <fstream>

int             main()
{
  Sery::Buffer  buffer;
  Sery::Stream  stream(buffer, Sery::LittleEndian);
  std::ifstream file("BinaryFile", std::ofstream::binary);
  Sery::int32   magic;
  std::string   str;

  Sery::readToBuffer([&file] (char* buf, int size) {
      file.read(buf, size);
    },
    buffer, 21);

  stream >> magic >> str;
  file.close();
  if (magic == 0x11223344)
    std::cout << "Magic ok!\n";
  else
    std::cerr << "Invalid magic!\n";
  return 0;
}
```

Build
------
_For now, **Sery** only supports static linking._<br><br>
To be able to build **Sery**, all you need is **CMake** and a **C++11 compiler**.
* Generate a project using CMake
* Linux:
    * `make` to compile **Sery**
    * `sudo make install` to install **Sery**
* Windows:
    * Open `Sery.sln` in Visual Studio
    * Build the project `ALL_BUILD` in `Debug` and/or `Release` depending on your needs
    * Build the project `INSTALL` in `Debug` and/or `Release` depending on your needs
* **Sery** is ready to use.

Linkage
------
**Sery** provides a `FindSery.cmake` module to help the user locate what he needs.<br>
This module will try to find the installed components needed to use **Sery**.<br>
To ensure it finds everything, the user can set the variable `SERY_ROOT` to the installation directory.<br>
If this variable is not set, the module will look under the following paths :
```cmake
C:/Program Files (x86)/sery
C:/Program Files/sery
/usr/local/
/usr/
/sw
/opt/local/
/opt/csw/
/opt/
```
And in the environment variable `SERY_ROOT`.

After attempting to find **Sery**, the module will set `SERY_FOUND` to either `TRUE` or `FALSE`.<br>
If **Sery** was found, some additional variables will be provided by the module:
* `SERY_INCLUDE_DIR`      - The include directory of Sery
* `SERY_LIBRARY`          - Link against this variable
* `SERY_LIBRARY_DEBUG`    - Contains the debug library if found. Otherwise, contains the release one.
* `SERY_LIBRARY_RELEASE`  - Contains the release library if found. Otherwise, contains the debug one.

#### Minimal CMake example
Given that **Sery** was installed in `D:/Libs/Sery`, a minimal example to link a target `test_sery` against **Sery** in a CMake file would be:
```cmake
# Set the root directory of Sery properly
SET(SERY_ROOT "D:/Libs/Sery")
# Tells CMake that it can look for additional modules here
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${SERY_ROOT}/cmake/")

# Attempts to find Sery
FIND_PACKAGE(Sery REQUIRED)
# If Sery was properly found
IF (SERY_FOUND)
  # Adds the include directory of Sery to the global include directories
  INCLUDE_DIRECTORIES(${SERY_INCLUDE_DIR})
  # Links test_sery against Sery
  TARGET_LINK_LIBRARIES(test_sery ${SERY_LIBRARY})
ENDIF(SERY_FOUND) 
```

Amalgamation
------
For convenience, each [release][releases-url] comes with a `Sery_Amalgamated.hh` file containing the whole library.<br>
This allows the user to simply add **Sery** to his project as if it was a part of the project's sources.<br>
Simply follow the instructions at the top of the amalgamated file to use it properly.

[travis-image]: https://img.shields.io/travis/Telokis/Sery/master.svg?label=linux
[travis-url]: https://travis-ci.org/Telokis/Sery
[appveyor-image]: https://img.shields.io/appveyor/ci/Ninetainedo/sery-46qsa/master.svg?label=windows
[appveyor-url]: https://ci.appveyor.com/project/Ninetainedo/sery-46qsa
[coveralls-image]: https://img.shields.io/coveralls/Telokis/Sery/master.svg
[coveralls-url]: https://coveralls.io/r/Telokis/Sery?branch=master
[releases-url]: https://github.com/Telokis/Sery/releases
