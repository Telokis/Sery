#include "gtest/gtest.h"
#include <Sery/Buffer.hh>
#include <Sery/Stream.hh>

TEST(SeryStream, int)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  int i1, i2;

  ASSERT_EQ(b.size(), 0)
    << "Initial size should be 0";
  i1 = 0x1865ff2b;
  stream << i1;
  ASSERT_EQ(b.size(), sizeof(i1))
    << "Size after int serialization should be sizeof(int)";
  stream >> i2;
  ASSERT_EQ(b.size(), 0)
    << "Size after int deserialization should be 0";
  ASSERT_TRUE(i1 == i2 && i2 == 0x1865ff2b)
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, bool)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  bool b1, b2;

  ASSERT_EQ(b.size(), 0)
    << "Initial size should be 0";
  b1 = false;
  stream << b1;
  ASSERT_EQ(b.size(), sizeof(b1))
    << "Size after int serialization should be sizeof(bool)";
  stream >> b2;
  ASSERT_EQ(b.size(), 0)
    << "Size after int deserialization should be 0";
  ASSERT_TRUE(b1 == b2 && b2 == false)
    << "Serialized and deserialized values should be the same.";
  stream << true;
  stream >> b2;
  ASSERT_TRUE(b2 == true)
    << "Serialized and deserialized values should be the same for rvalues.";
}

TEST(SeryStream, char)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  int c1, c2;

  c1 = '0';
  stream << c1;
  ASSERT_EQ(b.size(), sizeof(c1))
    << "Size after char serialization should be sizeof(char)";
  stream >> c2;
  ASSERT_EQ(b.size(), 0)
    << "Size after char deserialization should be 0";
  ASSERT_TRUE(c1 == c2 && c2 == '0')
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, double)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  double d1, d2;

  d1 = 3.1415926535;
  stream << d1;
  ASSERT_EQ(b.size(), sizeof(d1))
    << "Size after double serialization should be sizeof(double)";
  stream >> d2;
  ASSERT_EQ(b.size(), 0)
    << "Size after double deserialization should be 0";
  ASSERT_TRUE(d1 == d2 && d2 == 3.1415926535)
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, CString)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  char cstring[] = "Hello Foo bar";
  char* m1;
  char* m2;

  stream << cstring << "back";
  stream >> m1 >> m2;
  ASSERT_STREQ(cstring, m1)
    << "Serialized and deserialized values should be the same.";
  ASSERT_STREQ("back", m2)
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, Mixte)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  int i1, i2;
  double d1, d2;

  i1 = 0xdeadbeef;
  d1 = 1.4142135;
  stream << i1;
  stream << d1;
  ASSERT_EQ(b.size(), sizeof(i1) + sizeof(d1))
    << "Size after int and double serialization should be sizeof(int) + sizeof(double)";
  stream >> i2;
  stream >> d2;
  ASSERT_EQ(b.size(), 0)
    << "Size after deserialization should be 0";
  ASSERT_TRUE(i1 == i2 && i2 == 0xdeadbeef)
    << "Serialized and deserialized values should be the same.";
  ASSERT_TRUE(d1 == d2 && d2 == 1.4142135)
    << "Serialized and deserialized values should be the same.";
}

class DummySerializable
{
public:
  DummySerializable(Sery::int32 _i, double _d, Sery::uint64 _ul, Sery::int16 _s)
  {
    i = _i;
    d = _d;
    ulli = _ul;
    s = _s;
  };
  DummySerializable()
  {
    i = 0;
    d = 0;
    ulli = 0;
    s = 0;
  };
  Sery::Stream &write(Sery::Stream &out)
  {
    return (out << i << d << ulli << s);
  }
  Sery::Stream &read(Sery::Stream &in)
  {
    return (in >> i >> d >> ulli >> s);
  }
  bool  operator==(const DummySerializable &r) const
  {
    return (i == r.i && d == r.d && ulli == r.ulli && s == r.s);
  }
private:
  Sery::int32 i;
  double  d;
  Sery::uint64 ulli;
  Sery::int16 s;
};

TEST(SeryStream, CustomClass)
{
  DummySerializable dummy(34548795, 9858.1848951, 12154484959445L, 3545);
  DummySerializable dummy2;
  Sery::Buffer  b;
  Sery::Stream  stream(b);

  dummy.write(stream);
  dummy2.read(stream);

  ASSERT_EQ(dummy, dummy2)
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, Endianness)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  Sery::Stream  stream2(b, Sery::LittleEndian);
  Sery::int32 i1, i2, i3;

  i1 = 0x11223344;
  stream << i1;
  stream >> i2;
  stream << i1;
  stream2 >> i3;
  ASSERT_TRUE(i1 == i2 && i2 == 0x11223344)
    << "Serialized and deserialized values should be the same.";
  ASSERT_EQ(i3, 0x44332211)
    << "Serialized and deserialized values should be the same.";
  stream2 << i1;
  stream2 >> i3;
  ASSERT_TRUE(i1 == i3 && i3 == 0x11223344)
    << "Serialized and deserialized values should be the same.";
}

TEST(SeryStream, GlobalEndianness)
{
  Sery::Stream::setGlobalEndian(Sery::BigEndian);
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  ASSERT_EQ(stream.getLocalEndian(), Sery::Stream::getGlobalEndian())
    << "Local endian should be Global endian by default.";

  Sery::Stream::setGlobalEndian(Sery::LittleEndian);
  ASSERT_EQ(stream.getLocalEndian(), Sery::BigEndian)
    << "Local endian is not retroactively changed when global endian is changed.";

  Sery::Stream  stream2(b);
  ASSERT_EQ(stream2.getLocalEndian(), Sery::Stream::getGlobalEndian())
    << "Newly created streams should be based on the global endian.";

  ASSERT_EQ(Sery::Stream::getGlobalEndian(), Sery::LittleEndian)
    << "Global endian should be properly set.";

  Sery::Stream  stream3(b, Sery::BigEndian);
  ASSERT_EQ(stream3.getLocalEndian(), Sery::BigEndian)
    << "Specifying an endian inside the constructor should ignore the global one.";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
