#include "gtest/gtest.h"
#include <Sery/Buffer.hh>
#include <Sery/Stream.hh>

TEST(SeryStream_STD, string)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::string str1("FooBarALot0123456789TestEnd");
  std::string str2;

  stream << str1;
  stream >> str2;
  ASSERT_EQ(str1, str2)
    << "Serialized strings should be equals.";
}

TEST(SeryStream_STD, vector)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::vector<int> vec{ 3, 54, 87, -151, 87, 9, 6, 34, 5, 87, 84, 84 };
  std::vector<int> vec2;

  stream << vec;
  stream >> vec2;
  ASSERT_EQ(vec, vec2)
    << "Serialized vectors should be equals.";
}

TEST(SeryStream_STD, list)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::list<int> list{ 3, 54, 87, -151, 87, 9, 6, 34, 5, 87, 84, 84 };
  std::list<int> list2;

  stream << list;
  stream >> list2;
  ASSERT_EQ(list, list2)
    << "Serialized lists should be equals.";
}

TEST(SeryStream_STD, pair)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::pair<int, double> pair(-1254, 15698787.6548789);
  std::pair<int, double> pair2;

  stream << pair;
  stream >> pair2;
  ASSERT_EQ(pair, pair2)
    << "Serialized pairs should be equals.";
}

TEST(SeryStream_STD, map)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::map<int, double> map{
      { -1254, 15698787.6548789 },
      { 125487, 3214578.12021574 },
      { 36, -1987.3265 }
  };
  std::map<int, double> map2;

  stream << map;
  stream >> map2;
  ASSERT_EQ(map, map2)
    << "Serialized maps should be equals.";
}

TEST(SeryStream_STD, set)
{
  Sery::Buffer  b;
  Sery::Stream  stream(b);
  std::set<double> set{ -12455.1265, 626598.1587, 10146598.9484 };
  std::set<double> set2;

  stream << set;
  stream >> set2;
  ASSERT_EQ(set, set2)
    << "Serialized sets should be equals.";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);
  return (RUN_ALL_TESTS());
}
