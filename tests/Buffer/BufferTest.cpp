#include "gtest/gtest.h"
#include <sstream>
#include <functional>
#include <Sery/Buffer.hh>
#include <Sery/Stream.hh>
#include <cstring>

TEST(SeryBuffer, Basic)
{
  Sery::Buffer  buffer;
  char          out[5];

  buffer.writeRaw("tototo", 6);
  buffer.writeRaw("0123", 4);

  ASSERT_EQ(buffer.size(), 10)
    << "Sery::Buffer::size() didn't return as expected.";

  buffer.readRaw(out, 4);
  out[4] = 0;

  ASSERT_EQ(buffer.size(), 6)
    << "Sery::Buffer::size() didn't return as expected.";

  ASSERT_STREQ(out, "toto")
    << "Sery::Buffer::readRaw() didn't work as expected.";

  buffer.readRaw(out, 4);
  out[4] = 0;

  ASSERT_EQ(buffer.size(), 2)
    << "Sery::Buffer::size() didn't return as expected.";

  ASSERT_STREQ(out, "to01")
    << "Sery::Buffer::readRaw() didn't work as expected.";
}

TEST(SeryBuffer, ImportCharBuffer)
{
  char          in[] = "Imported buffer.";
  std::size_t   size = std::strlen(in) + 1;
  Sery::Buffer  buffer(in, size);

  ASSERT_EQ(buffer.size(), size)
    << "Sery::Buffer::size() didn't return as expected.";

  ASSERT_STREQ(buffer.data(), "Imported buffer.")
    << "Data should be equal to input string.";
}

namespace ph = std::placeholders;
using istringstream = std::istringstream;

TEST(SeryBuffer, ReadFromIStringStreamToBuffer)
{
  Sery::Buffer    buffer;
  istringstream   stream("Dummy Values");

  ASSERT_EQ(buffer.size(), 0)
    << "Buffer should start empty.";

  Sery::readToBuffer([&stream] (char* buf, int size) {
    stream.read(buf, size);
  },
    buffer, 5);

  ASSERT_EQ(buffer.size(), 5)
    << "Buffer should have read 5 chars.";

  Sery::readToBuffer(std::bind(&istringstream::read, &stream, ph::_1, ph::_2),
                     buffer, 7);

  ASSERT_EQ(buffer.size(), 7)
    << "Buffer should have read 7 chars.";
}

TEST(SeryBuffer, DebuggingBuffer)
{
  Sery::Buffer    buffer;
  istringstream   stream("Dummy Values");

  Sery::readToBuffer([&stream] (char* buf, int size) {
    stream.read(buf, size);
  },
    buffer, 12);

  auto str = buffer.debug(4);

  ASSERT_EQ(str, "44 75 6d 6d\n79 20 56 61\n6c 75 65 73")
    << "Debug should behave as expected";
}

int main(int ac, char** av)
{
  testing::InitGoogleTest(&ac, av);

  return (RUN_ALL_TESTS());
}
