#include <Sery/Buffer.hh>
#include <cassert>
#include <cstring>
#include <iomanip>
#include <sstream>

namespace Sery
{

Buffer::Buffer()
   
   
= default;

Buffer::Buffer(const char* buffer, std::size_t size)
  : _buffer(buffer, buffer + size)
  , _pos(0)
{
}

Buffer::~Buffer()
= default;

void        Buffer::writeRaw(const char* buffer, std::size_t size)
{
  _buffer.insert(_buffer.end(), buffer, buffer + size);
}

void        Buffer::readRaw(char* buffer, std::size_t size)
{
  memcpy(buffer, _buffer.data() + _pos, size);
  eraseNBytes(size);
}

std::size_t Buffer::size() const
{
  return _buffer.size() - _pos;
}

const char* Buffer::data() const
{
  return _buffer.data() + _pos;
}

void        Buffer::clear()
{
  _pos = 0;
  _buffer.clear();
}

void        Buffer::eraseNBytes(std::size_t num)
{
  _pos += num;
  if (_pos >= _buffer.size() / 2)
  {
    _buffer.erase(_buffer.begin(), _buffer.begin() + _pos);
    _pos = 0;
  }
}

void        Buffer::setContent(const char* buffer, std::size_t size)
{
  clear();
  _buffer.insert(_buffer.end(), buffer, buffer + size);
}

const std::string Buffer::debug(uint8 width) const
{
  assert(width > 0);
  const char        *data = this->data();
  std::stringstream stream;

  stream << std::setfill('0') << std::hex;
  for (uint32 i = 0; i < _buffer.size(); ++i)
  {
    uint32  k = i + 1;
    if (k > 1 && i % width != 0) {
      stream << ' ';
    
}uint8 c = data[i];
    stream << std::setw(2) << static_cast<unsigned int>(c);
    if (k % width == 0 && k < _buffer.size()) {
      stream << '\n';
  
}}
  return stream.str();
}

} // namespace Sery
