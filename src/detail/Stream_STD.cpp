#include <Sery/Stream.hh>

namespace Sery
{

// std::string
Stream& operator<<(Stream& stream, const std::string& str)
{
  stream << str.c_str();
  return (stream);
}

Stream& operator>>(Stream& stream, std::string& str)
{
  char* raw;

  stream >> raw;
  str = raw;
  return (stream);
}

} // namespace Sery
