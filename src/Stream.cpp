#include <Sery/IBuffer.hh>
#include <Sery/Stream.hh>
#include <cstring>
#include <iostream>

namespace Sery
{

Stream::Stream(IBuffer& buffer, Endian localEndian)
: _buffer(buffer),
  _localEndian(localEndian)
{
}

Stream::Stream(IBuffer& buffer)
: _buffer(buffer),
  _localEndian(globalEndian)
{
}

Stream&  Stream::writeRaw(const char* buffer, uint32 size)
{
  _buffer.writeRaw(buffer, size);
  return *this;
}

Stream&  Stream::readRaw(char* buffer, uint32 size)
{
  _buffer.readRaw(buffer, size);
  return *this;
}

Endian  Stream::getLocalEndian() const
{
  return _localEndian;
}

// Static members
  Endian Stream::globalEndian = Endian::BigEndian;

void     Stream::setGlobalEndian(Endian endian)
{
  globalEndian = endian;
}

Endian   Stream::getGlobalEndian()
{
  return globalEndian;
}

//////////////////////////////////////////
// External functions for serialization //
//////////////////////////////////////////

// Templates for serializing arithmetics types
template <class T, enable_if_t<std::is_arithmetic<T>::value>*>
Stream&   operator<<(Stream& stream, T t)
{
  Endian  softwareEndian  = detail::getSoftwareEndian();
  Endian  currentEndian   = stream.getLocalEndian();
  char    buffer[sizeof(T)];
  const auto* p = reinterpret_cast<const uint8*>(&t);

  for (size_t index = 0;
       index < sizeof(T);
       ++index)
  {
    if (currentEndian == softwareEndian) {
      buffer[index] = *p++;
    } else {
      buffer[sizeof(T) - index - 1] = *p++;
}
  }

  stream.writeRaw(buffer, sizeof(T));
  return stream;
}

// Explicit instantiations of templates functions
template Stream& operator<< <int8>        (Stream& , int8);
template Stream& operator<< <int16>       (Stream& , int16);
template Stream& operator<< <int32>       (Stream& , int32);
template Stream& operator<< <int64>       (Stream& , int64);
template Stream& operator<< <uint8>       (Stream& , uint8);
template Stream& operator<< <uint16>      (Stream& , uint16);
template Stream& operator<< <uint32>      (Stream& , uint32);
template Stream& operator<< <uint64>      (Stream& , uint64);
template Stream& operator<< <float>       (Stream& , float);
template Stream& operator<< <double>      (Stream& , double);
template Stream& operator<< <long double> (Stream& , long double);

  template <class T, enable_if_t<std::is_arithmetic<T>::value>*>
  Stream& Stream::operator>>(T& t)
  {
    Stream& stream = *this;
      Endian  softwareEndian  = detail::getSoftwareEndian();
      Endian  currentEndian   = stream.getLocalEndian();
      const char* buffer = stream._buffer.data();

      auto*  p = reinterpret_cast<uint8*>(&t);

      for (size_t index = 0;
	   index < sizeof(T);
	   ++index)
	{
	  if (currentEndian == softwareEndian) {
	    *p++ = buffer[index];
	  } else {
	    *p++ = buffer[sizeof(T) - index - 1];
}
	}

      stream._buffer.eraseNBytes(sizeof(T));
      return stream;
  }
  
// Explicit instantiations of templates functions
  template Stream& Stream::operator>> <int8>        (int8&);
  template Stream& Stream::operator>> <int16>       (int16&);
  template Stream& Stream::operator>> <int32>       (int32&);
  template Stream& Stream::operator>> <int64>       (int64&);
  template Stream& Stream::operator>> <uint8>       (uint8&);
  template Stream& Stream::operator>> <uint16>      (uint16&);
  template Stream& Stream::operator>> <uint32>      (uint32&);
  template Stream& Stream::operator>> <uint64>      (uint64&);
  template Stream& Stream::operator>> <float>       (float&);
  template Stream& Stream::operator>> <double>      (double&);
  template Stream& Stream::operator>> <long double> (long double&);

// (De)Serialization of C-Style strings
Stream&   operator<<(Stream& stream, const char* str)
{
  auto  len = static_cast<uint32>(std::strlen(str) + 1);

  stream << len;
  stream.writeRaw(str, len);
  return stream;
}
Stream&   operator>>(Stream& stream, char*& str)
{
  uint32  len = 0;

  stream >> len;
  auto* buffer = new char[len];
  stream.readRaw(buffer, len);
  str = buffer;
  return stream;
}

// (De)Serialization of boolean
Stream&   operator<<(Stream& stream, const bool value)
{
  stream << static_cast<const uint8>(value);
  return stream;
}

Stream&   operator>>(Stream& stream, bool& value)
{
  uint8   casted = 0;

  stream >> casted;
  value = casted != 0;
  return stream;
}

} // namespace Sery
