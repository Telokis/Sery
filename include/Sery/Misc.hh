#pragma once

#ifndef SERY_MISC_HH_
#define SERY_MISC_HH_

#include <cstdint>
#include <type_traits>

namespace Sery
{

/**
 *  @brief  Shortcut for using std::enable_if.
 */
template< bool B, class T = void >
using enable_if_t = typename ::std::enable_if<B, T>::type;

/**
 *  @brief  Shortcut for using std::result_of.
 */
template< class T >
using result_of_t = typename std::result_of<T>::type;

/**
 *  @brief  Enum used to represent little of big endian.
 */
enum Endian
{
  LittleEndian,
  BigEndian
};

using int8 = std::int8_t;   /**< Shortcut */
using int16 = std::int16_t;  /**< Shortcut */
using int32 = std::int32_t;  /**< Shortcut */
using int64 = std::int64_t;  /**< Shortcut */

using uint8 = std::uint8_t;  /**< Shortcut */
using uint16 = std::uint16_t; /**< Shortcut */
using uint32 = std::uint32_t; /**< Shortcut */
using uint64 = std::uint64_t; /**< Shortcut */


#ifndef _DOXYGEN

namespace       detail
{

  inline Endian getSoftwareEndian()
  {
    int16       witness = 0x5501;
    int8        test = *(reinterpret_cast<int8*>(&witness));
    return (test == 1 ? Endian::LittleEndian : Endian::BigEndian);
  }

} // namespace detail

#endif // _DOXYGEN

} // namespace Sery

#endif // SERY_MISC_HH_
